export class User {
	constructor(
		public id?: number,
		public name?: string,
		public surname?: string,
		public dni?: string,
		public email?: string,
		public roles?: string[],
		public permissions?: string[]
	) {}
}