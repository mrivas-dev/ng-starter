import { ChangeDetectorRef, HostListener, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { UtilService } from '../../../core';
import { AuthService } from '../../../services/auth.service';
import { Page, Section } from '../../../interfaces';
import { User } from '../../../models';
import { SettingsService } from '../../../services/settings.service';

@Component({
	selector: 'app-layout',
	templateUrl: './layout.component.html',
	styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

	@ViewChild('snav') snav: MatSidenav;

	@ViewChild('confNav') confNav: MatSidenav;

	public lastUrl: string;

	public user: User = this.utilService.getLS('user', true);

	public isDarkTheme: boolean = false;

	public themeColor: 'primary' | 'accent' | 'warn' = 'primary';

	public isMobile: boolean = false;

	public sections: Section[] = [
		{
			title: '',
			children: [

			]
		}
	];

	constructor(
		private settingService: SettingsService,
		private utilService: UtilService,
		private router: Router,
		private authService: AuthService
	) {
		this.themeColor = this.utilService.getLS('themeColor', true);
		this.isDarkTheme = this.utilService.getLS('isDarkTheme', true);
		if (this.snav) this.snav.opened = this.settingService.isSnavOpened;

		if (this.confNav) this.confNav.opened = this.settingService.isConfNavOpened;
	}

	ngOnInit(): void {

		if (document.documentElement.clientWidth <= 750) {
			this.isMobile = true;
			this.settingService.isSnavOpened = false;
			this.settingService.isConfNavOpened = false;
		}

		this.settingService.onSidenavToggle.subscribe(() => {
			if (!this.isMobile) {
				this.snav.opened = this.settingService.isSnavOpened;
			}
		});

		this.settingService.onConfNavToggle.subscribe(() => {
			this.confNav.opened = this.settingService.isConfNavOpened;
		});

		this.settingService.onDarkThemeToggle.subscribe(() => {
			this.isDarkTheme = this.settingService.isDarkTheme;
		});

		this.settingService.onPrimaryThemeSelected.subscribe(() => {
			this.themeColor = 'primary';
		});

		this.settingService.onWarnThemeSelected.subscribe(() => {
			console.log('warn')
			this.themeColor = 'warn';
		});

		this.settingService.onAccentThemeSelected.subscribe(() => {
			this.themeColor = 'accent';
		});

		setTimeout(() => {
			this.lastUrl = this.router.url;
			this.confNav.opened = this.settingService.isConfNavOpened;
		});

		setTimeout(() => {
			if (!this.isMobile) {
				this.snav.opened = this.settingService.isSnavOpened;
			}
		});

		this.renderLinks();
	}

	renderLinks() {
		this.authService.getPermissions().then(({ permission }) => {
			this.sections[0].children.push(
				{ url: `home`, title: 'Inicio', icon: 'web' },
			),
				this.sections[0].children = this.sections[0].children.filter(n => n);
		}).catch(console.log);
	}

	navigate(page) {
		if (page && page.url.indexOf("https") !== -1) {
			window.open(page.url, "_blank");
		} else {
			this.router.navigateByUrl(page.url);
			this.lastUrl = page.url;
			this.scrollToTop();
		}
	}

	scrollToTop() {
		const c = document.documentElement.scrollTop || document.body.scrollTop;
		if (c > 0) {
			window.requestAnimationFrame(() => {
				this.scrollToTop();
			});
			window.scrollTo(0, c - c / 8);
		}
	}

	isActive(page: Page) {
		return this.router.isActive(page.url, false);
	}

	isSettingButton({ icon }) {
		return !this.isMobile && icon === 'settings';
	}

	getMode() {
		return !this.isMobile ? 'side' : 'over';
	}

	@HostListener('window:resize', ['$event'])
	onResize(event) {
		this.isMobile = event.target.innerWidth < 750;
		if (this.snav) this.snav.opened = this.settingService.isSnavOpened;
	}
}