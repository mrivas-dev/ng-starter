import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { LayoutComponent } from './layout.component';
import { TopbarModule } from '../topbar/topbar.module';
import { SettingsModule } from '../settings/settings.module';
import { MatListModule } from '@angular/material/list';

@NgModule({
    declarations: [
        LayoutComponent
    ],
    imports: [
        CommonModule,
        MatSidenavModule,
        MatIconModule,
        MatButtonModule,
        MatListModule,
        TopbarModule,
        SettingsModule
    ],
    exports: [ LayoutComponent ],
    providers: []
})
export class LayoutModule { }
