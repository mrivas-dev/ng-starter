import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, OnInit } from '@angular/core';
import { MatRadioChange } from '@angular/material/radio';
import { UtilService } from '../../../services/util.service';
import { SettingsService } from '../../../services/settings.service';

enum ThemeColor {
  primary = 'primary',
  warn = 'warn',
  accent = 'accent'
}

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})

export class SettingsComponent implements OnInit {

  public themeColor: ThemeColor = ThemeColor.primary;

  public isDarkTheme: boolean = false;

  public themeList: string[] = ['primary', 'warn', 'accent'];

  constructor(
    private settingService: SettingsService,
    private utilService: UtilService,
    private overlayContainer: OverlayContainer
  ) {
    this.themeColor = this.utilService.getLS('themeColor', true);
    this.isDarkTheme = this.utilService.getLS('isDarkTheme', true);
  }

  ngOnInit(): void {

    this.settingService.onDarkThemeToggle.subscribe(() => {
      this.isDarkTheme = this.settingService.isDarkTheme;
    });

    this.settingService.onPrimaryThemeSelected.subscribe(() => {
      this.themeColor = ThemeColor.primary;
    });

    this.settingService.onWarnThemeSelected.subscribe(() => {
      this.themeColor = ThemeColor.warn;
    });

    this.settingService.onAccentThemeSelected.subscribe(() => {
      this.themeColor = ThemeColor.accent;
    });

  }

  public themeChange(event: MatRadioChange): void {
    switch (event.value) {
      case 'primary': this.selectPrimaryTheme(); break;
      case 'warn': this.selectWarnTheme(); break;
      case 'accent': this.selectAccentTheme(); break;
    }
  }

  public toggleMode(): void {
    this.toggleTheme();
  }

  public selectPrimaryTheme(): void {
    this.settingService.onPrimaryThemeSelected.emit();
  }

  public selectAccentTheme(): void {
    this.settingService.onAccentThemeSelected.emit();
  }

  public selectWarnTheme(): void {
    this.settingService.onWarnThemeSelected.emit();
  }

  public toggleTheme(): void {
    this.settingService.onDarkThemeToggle.emit();
    if (this.isDarkTheme) {
      this.overlayContainer.getContainerElement().classList.add('dark-theme');
    } else {
      this.overlayContainer
        .getContainerElement()
        .classList.remove('dark-theme');
    }
  }

  public getColor(): ThemeColor {
    switch (this.themeColor) {
      case 'primary': return ThemeColor.primary;
      case 'warn': return ThemeColor.warn;
      case 'accent': return ThemeColor.accent;
    }
  }

  public getThemeLabel(themeColor: string): string {
    switch (themeColor) {
      case 'primary': return this.isDarkTheme ? 'Primario oscuro' : 'Primario'
      case 'warn': return this.isDarkTheme ? 'Secundario oscuro' : 'Secundario'
      case 'accent': return this.isDarkTheme ? 'Terciario oscuro' : 'Terciario'
    }
  }

}
