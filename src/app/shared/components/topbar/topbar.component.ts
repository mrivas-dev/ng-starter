import { Component, OnInit } from '@angular/core';
import { HostListener } from '@angular/core';
import { User } from '../../../models';
import { Router } from '@angular/router';
import { UtilService } from '../../../services/util.service';
import { AuthService } from '../../../services/auth.service';
import { SettingsService } from '../../../services/settings.service';

@Component({
	selector: 'app-topbar',
	templateUrl: './topbar.component.html',
	styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {
	public themeColor: 'primary' | 'warn' | 'accent' = 'primary';

	public isDarkTheme: boolean = false;

	public user: User = this.utilService.getLS('user', true);

	public page: string = '';

	public isSnavOpened: boolean = false;

	public isConfNavOpened: boolean = false;

	public isMobile: boolean = false;

	constructor(
		private settingService: SettingsService,
		private utilService: UtilService,
		private authService: AuthService,
		private router: Router,
	) {
		this.themeColor = this.utilService.getLS('themeColor', true);
		this.isDarkTheme = this.utilService.getLS('isDarkTheme', true);
	}

	ngOnInit(): void {

		if (document.documentElement.clientWidth <= 750) {
			this.isMobile = true;
		}

		this.settingService.onSidenavToggle.subscribe(() => {
			this.isSnavOpened = this.settingService.isSnavOpened;
		});

		this.settingService.onConfNavToggle.subscribe(() => {
			this.isConfNavOpened = this.settingService.isConfNavOpened;
		});

		this.settingService.onDarkThemeToggle.subscribe(() => {
			this.isDarkTheme = this.settingService.isDarkTheme;
		});

		this.settingService.breadcrumbs.subscribe((page: string) => {
			this.page = page;
		});

		this.settingService.onPrimaryThemeSelected.subscribe(() => {
			this.themeColor = 'primary';
		});

		this.settingService.onWarnThemeSelected.subscribe(() => {
			this.themeColor = 'warn';
		});

		this.settingService.onAccentThemeSelected.subscribe(() => {
			this.themeColor = 'accent';
		});
	}

	public toggleSidenav() {
		this.settingService.onSidenavToggle.emit();
	}

	public toggleConfNav() {
		this.settingService.onConfNavToggle.emit();
	}

	public logout() {
		sessionStorage.clear();
		this.utilService.setLS("user", null, true);
		this.authService.logout();
		this.router.navigate(['/login']);
	}

	public goToProfile() {
		this.router.navigate([`perfil`]);
	}

	@HostListener('window:resize', ['$event'])
	onResize(event) {
		this.isMobile = event.target.innerWidth < 750;
	}
}
