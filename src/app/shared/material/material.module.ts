
import { NgModule } from '@angular/core';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRippleModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatRadioModule } from '@angular/material/radio';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonToggleModule } from '@angular/material/button-toggle';

@NgModule({
	imports:[
		MatFormFieldModule,
		MatButtonModule,
		MatInputModule,
		MatCardModule,
		MatProgressSpinnerModule,
		MatTooltipModule,
		MatSnackBarModule,
		MatDialogModule,
		MatToolbarModule,
		MatIconModule,
		MatCheckboxModule,
		MatSelectModule,
		MatTableModule,
		MatSidenavModule,
		MatListModule,
		MatMenuModule,
		MatRippleModule,
		MatSlideToggleModule,
		MatRadioModule,
		MatGridListModule,
		MatTabsModule,
		MatButtonToggleModule,
		
		DragDropModule,
	],
	exports: [
		MatFormFieldModule,
		MatButtonModule,
		MatInputModule,
		MatCardModule,
		MatProgressSpinnerModule,
		MatTooltipModule,
		MatSnackBarModule,
		MatDialogModule,
		MatToolbarModule,
		MatIconModule,
		MatCheckboxModule,
		MatSelectModule,
		MatTableModule,
		MatRadioModule,
		MatSidenavModule,
		MatListModule,
		MatMenuModule,
		MatGridListModule,
		MatRippleModule,
		MatSlideToggleModule,
		MatTabsModule,
		MatButtonToggleModule,
		DragDropModule
	],
})
export class MaterialModule {}