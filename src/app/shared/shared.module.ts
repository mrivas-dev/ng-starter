
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpService as HttpServiceComponents, HttpService } from '../core';

@NgModule({
	declarations: [ ],
	imports: [ CommonModule, MaterialModule, FormsModule, ReactiveFormsModule ],
	exports: [ CommonModule, MaterialModule, FormsModule, ReactiveFormsModule ],
	providers: [ { provide: HttpServiceComponents, useClass: HttpService } ]
})

export class SharedModule { }
