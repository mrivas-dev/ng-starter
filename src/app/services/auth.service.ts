import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { HttpService } from './http.service';
import { User } from '../models';

export interface Auth {
	access_token: string;
	expires_in: number;
	token_type: string;
	user: User;
}

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	private userIdentity: any;
	private authenticated = false;
	private authenticationState = new Subject<any>();

	constructor(private httpService: HttpService) { }

	login(username: string, password: string): Promise<any> {
		return new Promise((resolve, reject) => {
			return this.httpService.post('auth/login', { username, password }).then((auth: Auth) => {
				if (auth && auth.user) {
					this.authenticate(auth.user);
				} else {
					this.authenticate(null);
				}
				resolve(auth);
			}).catch(error => {
				reject(error);
			});
		});
	}

	impersonate(userId: string): Promise<any> {
		return new Promise((resolve, reject) => {
			return this.httpService.post(`auth/impersonate/${userId}`).then((auth: Auth) => {
				if (auth && auth.user) {
					this.authenticate(auth.user);
				} else {
					this.authenticate(null);
				}
				resolve(auth);
			}).catch(error => {
				reject(error);
			});
		});
	}

	logout() {
		return this.httpService.post('auth/logout').then(() => this.authenticate(null));
	}

	refresh() {
		return this.httpService.post('auth/refresh');
	}

	authenticate(user: User) {
		this.userIdentity = user;
		this.authenticated = user !== null;
		this.authenticationState.next(this.userIdentity);
	}

	hasAnyRole(roles: string[]): Promise<boolean> {
		return Promise.resolve(this.hasAnyRoleDirect(roles));
	}

	hasAnyRoleDirect(roles: string[]): boolean {
		if (!this.authenticated || !this.userIdentity || !this.userIdentity.roles) {
			return false;
		}

		for (let i = 0; i < roles.length; i++) {
			if (this.userIdentity.roles.includes(roles[i])) {
				return true;
			}
		}

		return false;
	}

	hasRole(role: string): Promise<boolean> {
		if (!this.authenticated) {			
			return Promise.resolve(false);
		}

		return this.identity().then(
			user => {
				return Promise.resolve(user.roles && user.roles.includes(role));
			},
			() => {
				return Promise.resolve(false);
			}
		);
	}

	can(permission: string): Promise<boolean> {
		if (!this.authenticated) {
			return Promise.resolve(false);
		}

		return this.identity().then(
			user => {
				return Promise.resolve(user.permissions && user.permissions.join(';').includes(permission));
			},
			() => {
				return Promise.resolve(false);
			}
		);
	}

	getPermissions() {
		return new Promise((resolve, reject) => {
			if (!this.authenticated) {
				reject();
			}
			
			this.identity(true).then(user => {
				resolve(user.permissions.join(';'));
			}, () => {
				reject();
			});
		});
	}

	identity(force?: boolean): Promise<User> {
		return new Promise((resolve, reject) => {
			if (force === true) {
				this.userIdentity = undefined;
			}

			// check and see if we have retrieved the userIdentity data from the server.
			// if we have, reuse it by immediately resolving
			if (this.userIdentity) {
				resolve(this.userIdentity);
				return;
			}

			// retrieve the userIdentity data from the server, update the identity object, and then resolve.
			this.httpService.get('auth/me')
				.then(account => {
					if (account) {
						this.userIdentity = account;
						this.authenticated = true;
					} else {
						this.userIdentity = null;
						this.authenticated = false;
					}
					this.authenticationState.next(this.userIdentity);
					resolve(this.userIdentity);
				})
				.catch(err => {
					this.userIdentity = null;
					this.authenticated = false;
					this.authenticationState.next(this.userIdentity);
					reject();
				});
		});
	}

	isAuthenticated(): boolean {
		return this.authenticated;
	}

	isIdentityResolved(): boolean {
		return this.userIdentity !== undefined;
	}

	getAuthenticationState(): Observable<any> {
		return this.authenticationState.asObservable();
	}
}
