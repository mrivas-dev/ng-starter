import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

/**
* Options to send in request to the BE. Includes header and credentials
*/
const urlOptions = {
	// withCredentials: true
};

@Injectable()
export class HttpService {

	constructor(
		private http: HttpClient,
		private snackBar: MatSnackBar
		) { }

		baseUrl = environment.baseUrl;

		autocomplete(url: string, data: any = null): Observable<any> {
			const options = data ? this.paramsRequest(data) : urlOptions;
			return this.http.get<any>(this.getUrl(url), options);
		}

		async get(url: string, data: any = null) {
			const options = data ? this.paramsRequest(data) : urlOptions;
			try {
				const res = await this.http.get<any>(this.getUrl(url), options).toPromise();
				return res;
			} catch (error) {
				throw new Error(this.error(error));
			}
		}

		async post(url: string, data: any = null) {
			try {
				const res = await this.http.post<any>(this.getUrl(url), data, urlOptions).toPromise();
				return res;
			} catch (error) {
				throw new Error(this.error(error));
			}
		}

		async put(url: string, data: any = null) {
			try {
				const res = await this.http.put<any>(this.getUrl(url), data, urlOptions).toPromise();
				return res;
			} catch (error) {
				throw new Error(this.error(error));
			}
		}

		async delete(url: string) {
			try {
				const res = await this.http.delete<any>(this.getUrl(url), urlOptions).toPromise();
				return res;
			} catch (error) {
				throw new Error(this.error(error));
			}
		}

		async file(url: string, formData: FormData) {
			try {
				const res = await this.http.post(this.getUrl(url), formData, urlOptions).toPromise();
				return res;
			} catch (error) {
				throw new Error(this.error(error));
			}
		}

		async getExcel(url: string) {
			try {
				const res = await this.http.get(this.getUrl(url), {responseType: 'blob'}).toPromise();
				return res;
			} catch (error) {
				throw new Error(this.error(error));
			}
		}

		upload(url: string, file: File): Observable<any> {
			const formData = new FormData();
			formData.append('file', file);
			return this.http.post<any>(this.baseUrl + url, formData, { reportProgress: true, observe: 'events' });
		}

		private getUrl(url: string) {
			if (url.indexOf('http') === 0) {
				return url;
			} else {
				return this.baseUrl + url;
			}
		}

		private paramsRequest(data: any) {
			let params = new HttpParams();
			for (const key in data) {
				if (data.hasOwnProperty(key)) {
					params = params.set(key, data[key]);
				}
			}
			return Object.assign({ params: params }, urlOptions);
		}

		private error(err) {
			console.error(err);
			if (!err.url.includes('auth')) {
				this.snackBar.open(err.error && err.error.error ? err.error.error : 'Error', null, {
					duration: 5000,
					horizontalPosition: 'end',
					verticalPosition: 'top'
				});
			}
			return err.error.message;
		}

	}
