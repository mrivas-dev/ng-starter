import { Injectable, EventEmitter } from '@angular/core';
import { UtilService } from './util.service';

@Injectable()
export class SettingsService {

    isSnavOpened: boolean = true;

    isConfNavOpened: boolean = false;

    isDarkTheme: boolean | null = null;

    themeColor: 'primary' | 'accent' | 'warn' = 'primary';

    constructor(private utilService: UtilService) {

        this.isDarkTheme = this.utilService.getLS('isDarkTheme', true);

        this.onSidenavToggle.subscribe(() => {
            this.isSnavOpened = !this.isSnavOpened;
        });

        this.onConfNavToggle.subscribe(() => {
            this.isConfNavOpened = !this.isConfNavOpened;
        })

        this.onDarkThemeToggle.subscribe(() => {
            this.isDarkTheme = !this.isDarkTheme;
            this.utilService.setLS('isDarkTheme', this.isDarkTheme, true);
        });

        this.onPrimaryThemeSelected.subscribe(() => {
            this.themeColor = 'primary';
            this.utilService.setLS('themeColor', this.themeColor, true);
        });

        this.onAccentThemeSelected.subscribe(() => {
            this.themeColor = 'accent';
            this.utilService.setLS('themeColor', this.themeColor, true);
        });

        this.onWarnThemeSelected.subscribe(() => {
            this.themeColor = 'warn';
            this.utilService.setLS('themeColor', this.themeColor, true);
        });
    }

    onSidenavToggle: EventEmitter<any> = new EventEmitter<any>();

    onConfNavToggle: EventEmitter<any> = new EventEmitter<any>();

    onDarkThemeToggle: EventEmitter<any> = new EventEmitter<any>();

    onPrimaryThemeSelected: EventEmitter<any> = new EventEmitter<'primary' | 'accent' | 'warn'>();

    onAccentThemeSelected: EventEmitter<any> = new EventEmitter<'primary' | 'accent' | 'warn'>();

    onWarnThemeSelected: EventEmitter<any> = new EventEmitter<'primary' | 'accent' | 'warn'>();

    breadcrumbs = new EventEmitter();

}
