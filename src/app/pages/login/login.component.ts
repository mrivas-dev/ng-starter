import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilService } from '../../core'
import { AuthService } from '../../services/auth.service';
import { SettingsService } from '../../services/settings.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	@ViewChild('password') passwordElement: ElementRef;

	public themeColorClass: 'primaryColorBG' | 'accentColorBG' | 'warnColorBG' = 'primaryColorBG';
	public isDarkTheme: boolean = false;

	public slowNetwork: boolean = false;
	public noConection: boolean = false;
	public loading: boolean = false;

	public isMobile: boolean = false;

	public loginForm = new FormGroup({
		username: new FormControl(null, [Validators.required, Validators.min(100000), Validators.max(999999999)]),
		password: new FormControl(null, [Validators.required]),
	});

	public showPass: boolean = false;

	public alert = { visible: false };

	private redirectUrl = undefined;

	private idParam = null;

	constructor(
		private activatedRoute: ActivatedRoute,
		private utilService: UtilService,
		private authService: AuthService,
		private router: Router,
		private settingService: SettingsService
	) {
		this.themeColorClass = this.setCustomClass(this.utilService.getLS('themeColor', true));
		this.isDarkTheme = this.utilService.getLS('isDarkTheme', true);
	}

	ngOnInit(): void {
		this.isMobile = document.documentElement.clientWidth <= 750;

		this.activatedRoute.queryParams.subscribe((params) => {
			this.redirectUrl = params["redirectUrl"];
			this.idParam = params["id"];
		});

		this.settingService.onDarkThemeToggle.subscribe(() => {
			this.isDarkTheme = this.settingService.isDarkTheme;
		});

		this.settingService.onPrimaryThemeSelected.subscribe(() => {
			this.themeColorClass = 'primaryColorBG';
		});

		this.settingService.onWarnThemeSelected.subscribe(() => {
			this.themeColorClass = 'warnColorBG';
		});

		this.settingService.onAccentThemeSelected.subscribe(() => {

			this.themeColorClass = 'accentColorBG';
		});
	}

	public reset() {
		this.loginForm.reset();
	}

	public loginDisabled() {
		return !this.loginForm.value.username || !this.loginForm.value.password || this.loading
	}

	public login() {
		const login = this.loginForm.value;
		this.loading = true;
		this.loginForm.controls['username'].disable();
		this.loginForm.controls['password'].disable();
		const notification = this.utilService.notification("Iniciando sesion...", "", 5);

		setTimeout(() => {
			if (this.loading === true) {
				if (window.navigator.onLine) {
					this.slowNetwork = true;
				} else {
					this.noConection = true;
				}
			}
		}, 5000, this.loading);

		this.authService
			.login(login.username, login.password)
			.then((data) => {
				if (data && data.user) {
					this.utilService.setLS("user", data.user, true);

					if (!this.utilService.getLS('themeColor', true)) {
						this.utilService.setLS('themeColor', '"primary"');
					}
					if (!this.utilService.getLS('isDarkTheme', true)) {
						this.utilService.setLS('isDarkTheme', 'false');
					}

					this.utilService.saveAuth(data);
					notification.dismiss();
					this.loading = false;
					this.slowNetwork = false;
					this.noConection = false;

					const redirection = this.redirectUrl
						? `${this.redirectUrl}${this.idParam ? `/${this.idParam}` : ''}`
						: '';
					this.router.navigate([redirection]);
				}
			})
			.catch((error) => {
				this.loading = false;
				this.slowNetwork = false;
				this.loginForm.controls['username'].enable();
				this.loginForm.controls['password'].enable();
				if (window.navigator.onLine) {
					this.noConection = false;
					this.utilService.notification('Contraseña incorrecta');
					this.focusPassword();
					this.resetPassword();
				} else {
					this.noConection = true;
				}
			});
	}

	public resetPassword() {
		this.alert.visible = true;
	}

	private focusPassword() {
		setTimeout(() => { // this will make the execution after the above boolean has changed
			this.passwordElement.nativeElement.focus();
		}, 0);
	}

	public hideAlert() {
		this.alert.visible = false;
	}

	public getBackground() {
		const imageUrl = `assets/img/login/login${this.isMobile ? 'Portrait' : ''}.png`;
		return { backgroundImage: `url("${imageUrl}")` }
	}

	private setCustomClass(theme: 'primary' | 'warn' | 'accent') {
		switch (theme) {
			case 'primary': return 'primaryColorBG';
			case 'warn': return 'warnColorBG';
			case 'accent': return 'accentColorBG';
		}
	}

	@HostListener('window:resize', ['$event'])
	onResize(event): void {
		this.isMobile = event.target.innerWidth < 750;
	}

}
