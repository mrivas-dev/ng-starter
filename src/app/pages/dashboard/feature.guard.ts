import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { AuthService } from '@app/services/auth.service';

@Injectable({
	providedIn: 'root'
})
export class FeatureGuard implements CanActivate {

	constructor(
		private authService: AuthService,
		private router: Router
	) { }
	async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		const user = await this.authService.identity();
		if (next.data.user.some(someUser => someUser.email === user.email)) {
			return true;
		} else {
			this.router.navigate(['404']);
		}

	}
}
