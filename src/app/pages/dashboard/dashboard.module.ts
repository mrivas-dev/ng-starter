import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { DashboardService } from './dashboard.service';
import { MatRippleModule } from '@angular/material/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { LayoutModule } from '../../shared/components/layout/layout.module';
import { TopbarModule } from '../../shared/components/topbar/topbar.module';
import { SettingsModule } from '../../shared/components/settings/settings.module';

@NgModule({
	declarations: [
		DashboardComponent
	],
	imports: [
		CommonModule,
		DashboardRoutingModule,
		SharedModule,
		LayoutModule,
		TopbarModule,
		SettingsModule,
		MatRippleModule
	],
	providers: [
		DashboardService
	]
})
export class DashboardModule { }
