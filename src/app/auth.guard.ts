import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService, UtilService } from './core';
import { AuthService } from './services/auth.service';

@Injectable({
	providedIn: 'root'
})
export class AuthGuard implements CanActivate {

	constructor(
		private httpService: HttpService,
		private router: Router,
		private utilService: UtilService,
		private authService: AuthService
	) { }

	canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
		return new Promise(resolve => {
			this.authService.identity().then(() => {
				resolve(true);
			}).catch(() => {
				this.utilService.notification('Su sesión ha expirado. Ingrese nuevamente por favor.');
				const redirectionUrl = state.url.charAt(0) === '/'
					? state.url.substring(1)
					: state.url
				this.router.navigateByUrl(`/login?redirectUrl=${redirectionUrl}`);
				resolve(false);
			});
		})
	}
}
