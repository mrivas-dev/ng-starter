import { NgModule } from '@angular/core';
import { LoginInterceptor } from './login.interceptor';
import { HTTP_INTERCEPTORS, HttpUrlEncodingCodec, HttpClientModule } from '@angular/common/http';
import { HttpService } from '../services/http.service';
import { UtilService } from '../services/util.service';
import { TokenInterceptor } from './token.interceptor';

@NgModule({
	declarations: [
	],
	imports: [
		HttpClientModule,
		
	],
	providers: [
		HttpService,
		UtilService,
		HttpUrlEncodingCodec,
		{ provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
		{ provide: HTTP_INTERCEPTORS, useClass: LoginInterceptor, multi: true }
	]
})
export class CoreModule { }
