import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
	providedIn: 'root'
})
export class PermissionGuard implements CanActivate {

	constructor(
		private authService: AuthService,
		private router: Router
	) { }
	async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		const user = await this.authService.identity();
		const permissions = next.data.permissions;
		const userPermissions = user.permissions;
		if ((!permissions || !permissions.length) || userPermissions.some(eachPermission => permissions.find(wantedPermission => wantedPermission === eachPermission))) {
			return true;
		} else {
			this.router.navigate(['404']);
		}
	}
}
